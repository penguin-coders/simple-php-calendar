<?php

/****************************************************************************
 * Simple PHP Calendar                                                      *
 * Copyright (C) 2021 Mark Williams                                         *
 *                                                                          *
 * This file is part of Simple PHP Calendar.                                *
 *                                                                          *
 * Simple PHP Calendar is free software: you can redistribute it and/or     *
 * modify it under the terms of the GNU General Public License as           *
 * published by the Free Software Foundation, either version 3 of the       *
 * License, or (at your option) any later version.                          *
 *                                                                          *
 * Simple PHP Calendar is distributed in the hope that it will be useful,   *         
 * but WITHOUT ANY WARRANTY; without even the implied warranty of           *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU         *
 * General Public License for more details.                                 *
 *                                                                          *
 * You should have received a copy of the GNU General Public licenses along *
 * with Simple PHP Calendar. If not, see <https://www.gnu.org/licenses/>.   *
 ****************************************************************************/

require_once("Calendar.php");
$title = "Simple PHP Calendar";

if(isset($_GET['m']) && isset($_GET['y'])) {
	$m = intval($_GET['m']);
	$y = intval($_GET['y']);
	$cal = new Calendar($m,$y);
} else {
	$cal = new Calendar();
}

if(isset($_GET['d'])) {
	$d = intval($_GET['d']);
	$dt = new DateTime();
	$dt->setDate($y,$m,$d);
	$title .= $dt->format(" - D d M Y");
}

?>

<!DOCTYPE html>
<html>
<head>
<title><?php print $title; ?></title>
<style>
body { font-family: Arial, Helvetica, sans-serif; }
.prev_month, .next_month { color: #bbb; }
</style>
</head>
<body>

<h1><?php print $title; ?></h1>

<?php

print $cal->mk_HTML();

if(isset($d)) {
}

?>
</body>
</html> 
