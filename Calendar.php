<?php

/****************************************************************************
 * Simple PHP Calendar                                                      *
 * Copyright (C) 2021 Mark Williams                                         *
 *                                                                          *
 * This file is part of Simple PHP Calendar.                                *
 *                                                                          *
 * Simple PHP Calendar is free software: you can redistribute it and/or     *
 * modify it under the terms of the GNU General Public License as           *
 * published by the Free Software Foundation, either version 3 of the       *
 * License, or (at your option) any later version.                          *
 *                                                                          *
 * Simple PHP Calendar is distributed in the hope that it will be useful,   *         
 * but WITHOUT ANY WARRANTY; without even the implied warranty of           *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU         *
 * General Public License for more details.                                 *
 *                                                                          *
 * You should have received a copy of the GNU General Public licenses along *
 * with Simple PHP Calendar. If not, see <https://www.gnu.org/licenses/>.   *
 ****************************************************************************/

error_reporting(E_ALL);

class Calendar {

	private $month;
	private $year;
	private $first_of_month;
	
	private $cal_start_date;
	private $lead_days;
	private $num_weeks;

	private $dt;
	private $oneday;
	private $css_classes;

	private $prev_next_links;
	private $prev_next_url;
	private $day_links;
	private $day_url;

	function __construct($month = "", $year = "", $show_links = true) {

		// Default to today's date if month / year not  passed in
		$this->month = ($month=='') ? date('m') : $month;
		$this->year = ($year=='') ? date('Y') : $year;

		// Store date of first day of month
		$this->first_of_month = new DateTime();
		$this->first_of_month->setDate($this->year,$this->month,1);

		// Work out lead days, start date for calendar, & number of weeks to display
		$this->lead_days = $this->first_of_month->format('N')-1;
		$this->cal_start_date = clone $this->first_of_month;
		$this->cal_start_date->sub(new DateInterval("P".$this->lead_days."D"));
		$this->num_weeks = ceil(($this->lead_days + $this->first_of_month->format('t')) / 7);

		// Initialise other vars
		$this->oneday = new DateInterval("P1D");
		$this->reset_dt();

		// Enable links & default urls to request uri
		if($show_links) {
			$this->set_prev_next_url($_SERVER['REQUEST_URI']);
			$this->enable_prev_next();
			$this->set_day_url($_SERVER['REQUEST_URI']);
			$this->enable_day_links();
		}

		// CSS classes
		$this->dt = clone $this->first_of_month;
		$this->css_classes[$this->dt->format('M')] = "curr_month";
		$this->dt->sub(new DateInterval("P1M"));
		$this->css_classes[$this->dt->format('M')] = "prev_month";
		$this->dt->add(new DateInterval("P2M"));
		$this->css_classes[$this->dt->format('M')] = "next_month";
		$this->reset_dt();
	}

	public function set_prev_next_url($url) {
		$url = explode("?",$url); // split url from query string
		$this->prev_next_url = $url[0];			
	}

	public function enable_prev_next() {
		$this->prev_next_links = true;
	}

	public function disable_prev_next() {
		$this->prev_next_links = false;
	}

	public function set_day_url($url) {
		$url = explode("?",$url); // split url from query string
		$this->day_url = $url[0];			
	}

	public function enable_day_links() {
		$this->day_links = true;
	}

	public function disable_day_links() {
		$this->day_links = false;
	}

	private function reset_dt() {
		$this->dt = clone $this->cal_start_date;
	}

	private function th() {
		return "<th>".substr($this->dt->format('D'),0,2)."</th>\n";
	}

	private function td() {
		if($this->day_links && $this->dt->format('m')==$this->month) {
			$td_inner = $this->mk_link("day",$this->dt->format('d'));
		} else {
			$td_inner = $this->dt->format('d');
		}
		return "<td class='".$this->css_classes[$this->dt->format('M')]."'>".$td_inner."</td>\n";
	}

	private function mk_week($mk_Day = "td") {

		$html = "<tr>";
		for($day=0; $day<7; $day++, $this->dt->add($this->oneday)) {
			$html .= $this->$mk_Day();
		}
		$html .= "</tr>";

		return $html;
	}	

	private function mk_link($type, $text) {

		switch($type) { 
		case "prev":
			$dt = clone $this->first_of_month;
			$dt->sub(new DateInterval("P1M"));
			break;
		case "next":
			$dt = clone $this->first_of_month;
			$dt->add(new DateInterval("P1M"));
			break;
		case "day":
			$dt = clone $this->dt;
		}
		
		$m = $dt->format("m");
		$y = $dt->format("Y");

		if($type=="day") { 
			$d = $dt->format("d");
			$link = "<a href='".$this->day_url."?d=".$d."&m=".$m."&y=".$y."'>".$text."</a>";
		} else {
			$link = "<a href='".$this->prev_next_url."?m=".$m."&y=".$y."'>".$text."</a>";
		}

		return $link;
	}

	private function mk_table_header() {

		$curr = " ".$this->first_of_month->format("M Y")." ";

		if($this->prev_next_links) {
			$prev = $this->mk_link("prev","Prev");
			$next = $this->mk_link("next","Next");
		} else {
			$prev = ""; $next == "";
		}
		return "<tr><th colspan='7'>".$prev.$curr.$next."</th></tr>";
	}

	public function mk_HTML() {

		$this->reset_dt();
		
		$html = "<table>";
		$html .= $this->mk_table_header();
		$html .= $this->mk_week("th");
		$this->reset_dt();

		for($w=0; $w<$this->num_weeks; $w++) { 
			$html .= $this->mk_week();
		}

		$html .= "</table>";

		$this->reset_dt();
		return $html;
	}
}

